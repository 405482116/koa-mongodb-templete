
import * as dotenv from 'dotenv';

dotenv.config();

export type TConfig = {
    DB_CONN_STRING: string,
    DB_NAME: string,
    COLLECTION_NAME: string,
    PORT: number,
    HOST: string
}
export const config: TConfig = {
    DB_CONN_STRING: process.env.DB_CONN_STRING || '<your db uri here>',
    DB_NAME: process.env.DB_NAME || '<your db name here>',
    COLLECTION_NAME: process.env.COLLECTION_NAME || '<your db collection name>',
    PORT: Number(process.env.PORT) || 8080,
    HOST: process.env.HOST || 'localhost'
};