import { MongoClient, Collection, Db } from 'mongodb';
import { config } from '../config';
const { DB_CONN_STRING, COLLECTION_NAME } = config;

export let collections: Collection | null;
export const connectToDatabase = async () => {
    const clinet: MongoClient = new MongoClient(DB_CONN_STRING);
    await clinet.connect();
    const db: Db = clinet.db(process.env.DB_NAME);
    const collection: Collection = db.collection(COLLECTION_NAME);

    collections = collection;
    console.log(`Successfully connected to database: ${db.databaseName} and collection: ${collection.collectionName}`);
};