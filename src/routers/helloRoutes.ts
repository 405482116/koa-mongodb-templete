import { Context, Middleware } from 'koa';

type TRouter = {
    method: 'get' | 'delete' | 'post';
    path: string;
    handler: Middleware
}

export const helloRoutes: TRouter = {
    method: 'get',
    path: '/hello',
    handler: async (ctx: Context) => {
        ctx.status = 200;
        ctx.body = {
            message: 'hello world'
        };
    }
};