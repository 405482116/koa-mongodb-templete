import koa from 'koa';
import Router from '@koa/router';
import koaBody from 'koa-body';
import { routers } from './routers';
import { connectToDatabase, collections } from './db';
import { config } from './config';


const { PORT, HOST } = config;
const app = new koa();
const router = new Router();


app.use(koaBody());


routers.map(_router => {
    router[_router.method](_router.path, _router.handler);
});

app.use(router.routes()).use(router.allowedMethods());
const run = async () => {
    try {
        await connectToDatabase();
        const result = await collections?.find({}).toArray();
        console.log(123, result);
        app.listen(PORT, HOST, () => {
            console.log(`⚡️[server]: Server is running at https://${HOST}:${PORT}`);
        });
    } catch (error) {
        console.error('Database connection failed', error);
        process.exit();
    }
};

run();